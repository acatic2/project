const Sequelize = require("sequelize");
const sequelize = new Sequelize("wt2117253", "root","password",{
    logging:false,
    host:"localhost",
    dialect:"mysql"
});
const baza={};
baza.Sequelize = Sequelize;
baza.sequelize = sequelize;

baza.Student = require('./models/student.js')(sequelize);
baza.Grupa = require('./models/grupa.js')(sequelize);
baza.Vjezba = require('./models/vjezba.js')(sequelize);
baza.Zadatak = require('./models/zadatak.js')(sequelize);

baza.Grupa.hasMany(baza.Student, {foreignKey: 'grupa'});
baza.Vjezba.hasMany(baza.Zadatak, {foreignKey: 'vjezba'});

module.exports=baza;