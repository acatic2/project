document.getElementById("posalji").addEventListener('click', function(){
    let rezultat = document.getElementById("ajaxStatus");
    rezultat.innerHTML = "Ajax status: ";
    let csvStudenti = document.getElementById("area").value;
    console.log(csvStudenti);
    StudentAjax.dodajBatch(csvStudenti, function(err, data) {
        if (data!=null)
            rezultat.innerHTML += JSON.stringify(data);
        else rezultat.innerHTML += JSON.stringify(err);
    });
});