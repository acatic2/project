document.getElementById("postavi").addEventListener('click', function(){
    let rezultat = document.getElementById("ajaxStatus");
    rezultat.innerHTML = "Ajax status: ";
    let index = document.getElementById("index").value;
    let grupa = document.getElementById("grupa").value;
    StudentAjax.postaviGrupu(index, grupa, function(err, data) {
        if (data!=null)
        rezultat.innerHTML += JSON.stringify(data) ;
        else rezultat.innerHTML += JSON.stringify(err) ;
    });
});