let StudentAjax = (function(){
    const dodajStudenta = function(student, fnCallback) {
    /*    let ime = student.ime, prezime = student.prezime, index = student.index, grupa = student.grupa;
            if (ime=="" || prezime=="" || index=="" || grupa=="" ||
                ime==null || prezime==null || index==null || grupa==null) {
                console.log("Neispravni podaci");
                return;
        }*/

        $.ajax({
            url:"http://localhost:3000/student",
            type: "POST",
            data: student,
            dataType: "json",
            success: function(podaci){
                fnCallback(null, podaci);
            }, 
            failure: function(greska) {
                fnCallback(greska, null);
            }
    
        });   
    }
    const postaviGrupu = function(index, grupa, fnCallback) {
      /*          if (index=="" || grupa=="" || index==null || grupa==null) {
                console.log("Neispravni podaci");
                return;
        }*/
        let podatak = {grupa:grupa};

        $.ajax({
            url:"http://localhost:3000/student/"+index,
            type: "PUT",
            data: podatak,
            dataType: "json",
            success: function(podaci){
                fnCallback(null, podaci);
            }, 
            failure: function(greska) {
                fnCallback(greska, null);
            }
    
        });   
        }
    const dodajBatch = function(csvStudenti, fnCallback) {
      /*  let nizStudenata = csvStudenti.split("\r\n");
        for (let i = 0; i<nizStudenata.length; i++) {
            let student = nizStudenata.split(",");
            let ime = student[0], prezime = student[1], index = student[2], grupa = student[3];
            if (ime=="" || prezime=="" || index=="" || grupa=="" ||
                ime==null || prezime==null || index==null || grupa==null) {
                console.log("Neispravni podaci");
                return;
        }
        }*/

    $.ajax({
        url:"http://localhost:3000/batch/student",
        type: "POST",
        data: csvStudenti,
        contentType: 'text/plain',
        success: function(podaci){
            fnCallback(null, podaci);
        }, 
        failure: function(greska) {
            fnCallback(greska, null);
        }

    });   
        }
        return{
            dodajStudenta : dodajStudenta,
            postaviGrupu : postaviGrupu,
            dodajBatch : dodajBatch
        }
    }());
