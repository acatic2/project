let assert = chai.assert;
describe('TestoviParser', function() {
 describe('dajTacnost()', function() {
   it('Svi testovi prolaze', function() {
     let rezultat1 = TestoviParser.dajTacnost("{\"stats\":{\"suites\":2,\"tests\":2,\"passes\":2,\"pending\":0,"
     +"\"failures\":0,\"start\":\"2021-11-05T15:00:26.343Z\",\"end\":\"2021-11-05T15:00:26.352Z\",\"duration\":9"
    +"},\"tests\":[{\"title\":\"should draw 3 rows when parameter are 2,3\",\"fullTitle\":"
    +"\"Tabela crtaj() should draw 3 rows when parameter are 2,3\",\"file\":null,\"duration\":1,"
    +"\"currentRetry\":0,\"speed\":\"fast\",\"err\":{}},{\"title\":\"should draw 2 columns in row 2 when "
    +"parameter are 2,3\",\"fullTitle\":\"Tabela crtaj() should draw 2 columns in row 2 when parameter are 2,3"
    +"\",\"file\":null,\"duration\":0,\"currentRetry\":0,\"speed\":\"fast\",\"err\":{}}],\"pending\":[],"
    +"\"failures\":[],\"passes\":[{\"title\":\"should draw 3 rows when parameter are 2,3\",\"fullTitle\":"
    +"\"Tabela crtaj() should draw 3 rows when parameter are 2,3\",\"file\":null,\"duration\":1,"
    +"\"currentRetry\":0,\"speed\":\"fast\",\"err\":{}},{\"title\":\"should draw 2 columns in row 2 when "
    +"parameter are 2,3\",\"fullTitle\":\"Tabela crtaj() should draw 2 columns in row 2 when parameter "
    +"are 2,3\",\"file\":null,\"duration\":0,\"currentRetry\":0,\"speed\":\"fast\",\"err\":{}}]}");
     let objekat1 = {"tacnost":"100%","greske":[]};
     assert.deepEqual(objekat1,rezultat1,"Tacnost treba biti 100%");
   });
   it('Testovi se ne mogu izvrsiti', function(){
     let rezultat2 = TestoviParser.dajTacnost("String se ne moze pretvoriti u JSON objekat!");
     let objekat2 = {"tacnost":"0%", "greske":["Testovi se ne mogu izvršiti"]};
     assert.deepEqual(objekat2,rezultat2,"Testovi se ne mogu izvrsiti");
   });
   it('Jedan test prolazi', function(){
     let rezultat3 = TestoviParser.dajTacnost("{\"stats\":{\"suites\":2,\"tests\":3,\"passes\":1,\"pending\":0,"
     +"\"failures\":2,\"start\":\"2021-11-24T11:19:43.804Z\",\"end\":\"2021-11-24T11:19:43.815Z\","
     +"\"duration\":11},\"tests\":[{\"title\":\"Svitestoviprolaze\",\"fullTitle\":\"TestoviParserdajTacnost()"
     +"Svitestoviprolaze\",\"file\":null,\"duration\":3,\"currentRetry\":0,\"err\":{\"message\":"
     +"\"Tacnosttrebabiti100%:expected{tacnost:'0%',greske:[]}todeeplyequal{tacnost:'100%',greske:[]}\","
     +"\"showDiff\":true,\"actual\":\"{\\n\\\"greske\\\":[]\\n\\\"tacnost\\\":\\\"0%\\\"\\n}\",\"expected\":"
     +"\"{\\n\\\"greske\\\":[]\\n\\\"tacnost\\\":\\\"100%\\\"\\n}\",\"operator\":\"deepStrictEqual\","
     +"\"stack\":\"AssertionError@https:\/\/unpkg.com\/chai\/chai.js:9574:13\\n[3]<\/module.exports\/Assertion"
     +".prototype.assert@https:\/\/unpkg.com\/chai\/chai.js:250:13\\nassertEql@https:\/\/unpkg.com"
     +"zadatak2.js:18:13\\n@file:\/\/\/zadatak2.html:22:12\\n\"}},{\"title\":\"Testovisenemoguizvrsiti\","
     +"\"fullTitle\":\"TestoviParserdajTacnost()Testovisenemoguizvrsiti\",\"file\":null,\"duration\":1,"
     +"\"currentRetry\":0,\"err\":{\"message\":\"Testovisenemoguizvrsiti:expected{Object(tacnost,greske)}"
     +"todeeplyequal{Object(tacnost,greske)}\"}},{\"title\":\"Jedantestprolazi\",\"fullTitle\":"
     +"\"TestoviParserdajTacnost()Jedantestprolazi\",\"file\":null,\"duration\":0,\"currentRetry\":0,"
     +"\"err\":{}}],\"pending\":[],"
     +"\"failures\":[{\"title\":\"Svitestoviprolaze\",\"fullTitle\":\"TestoviParserdajTacnost()"
     +"Svitestoviprolaze\",\"file\":null,\"duration\":3,\"currentRetry\":0,\"err\":{\"message\":"
     +"\"Tacnosttrebabiti100%:expected{tacnost:'0%',greske:[]}todeeplyequal{tacnost:'100%',greske:[]}\"}},"
     +"{\"title\":\"Testovisenemoguizvrsiti\",\"fullTitle\":\"TestoviParserdajTacnost()Testovisenemoguizvrsiti"
     +"\",\"file\":null,\"duration\":1,\"currentRetry\":0,\"err\":{\"message\":\"Testovisenemoguizvrsiti:"
     +"expected{Object(tacnost,greske)}todeeplyequal{Object(tacnost,greske)}\"}}],"
     +"\"passes\":[{\"title\":\"Jedantestprolazi\""
     +",\"fullTitle\":\"TestoviParserdajTacnost()Jedantestprolazi\",\"file\":null,\"duration\":0,"
     +"\"currentRetry\":0,\"err\":{}}]}");
     let objekat3 = {"tacnost": "33.3%", "greske":["TestoviParserdajTacnost()Svitestoviprolaze",
     "TestoviParserdajTacnost()Testovisenemoguizvrsiti"]};
     assert.equal(objekat3.tacnost, rezultat3.tacnost, "Tacnost treba biti 33.3%");
     assert.equal(rezultat3.tacnost,"33.3%","Tacnost treba biti 33.3%");
     assert.deepEqual(objekat3.greske[0],rezultat3.greske[0],"Jednak prvi clan niza");
     assert.deepEqual(objekat3.greske,rezultat3.greske,"Greske trebaju biti jednake");
   });
   it('Svi testovi padaju', function(){
     let rezultat4 = TestoviParser.dajTacnost("{\"stats\":{\"suites\":2,\"tests\":3,\"passes\":0,\"pending\":0,"
     +"\"failures\":3,\"start\":\"2021-11-24T11:19:43.804Z\",\"end\":\"2021-11-24T11:19:43.815Z\","
     +"\"duration\":11},\"tests\":[{\"title\":\"Svitestoviprolaze\",\"fullTitle\":\"TestoviParserdajTacnost()"
     +"Svitestoviprolaze\",\"file\":null,\"duration\":3,\"currentRetry\":0,\"err\":{\"message\":"
     +"\"Tacnosttrebabiti100%:expected{tacnost:'0%',greske:[]}todeeplyequal{tacnost:'100%',greske:[]}\","
     +"\"showDiff\":true,\"actual\":\"{\\n\\\"greske\\\":[]\\n\\\"tacnost\\\":\\\"0%\\\"\\n}\",\"expected\":"
     +"\"{\\n\\\"greske\\\":[]\\n\\\"tacnost\\\":\\\"100%\\\"\\n}\",\"operator\":\"deepStrictEqual\","
     +"\"stack\":\"AssertionError@https:\/\/unpkg.com\/chai\/chai.js:9574:13\\n[3]<\/module.exports\/Assertion"
     +".prototype.assert@https:\/\/unpkg.com\/chai\/chai.js:250:13\\nassertEql@https:\/\/unpkg.com"
     +"zadatak2.js:18:13\\n@file:\/\/\/zadatak2.html:22:12\\n\"}},{\"title\":\"Testovisenemoguizvrsiti\","
     +"\"fullTitle\":\"TestoviParserdajTacnost()Testovisenemoguizvrsiti\",\"file\":null,\"duration\":1,"
     +"\"currentRetry\":0,\"err\":{\"message\":\"Testovisenemoguizvrsiti:expected{Object(tacnost,greske)}"
     +"todeeplyequal{Object(tacnost,greske)}\"}},{\"title\":\"Jedantestprolazi\",\"fullTitle\":"
     +"\"TestoviParserdajTacnost()Jedantestprolazi\",\"file\":null,\"duration\":0,\"currentRetry\":0,"
     +"\"err\":{\"message\":\"Tacnosttrebabiti50%:expected'33.5%'toequal'33.3%'\"}}],\"pending\":[],"
     +"\"failures\":[{\"title\":\"Svitestoviprolaze\",\"fullTitle\":\"TestoviParserdajTacnost()"
     +"Svitestoviprolaze\",\"file\":null,\"duration\":3,\"currentRetry\":0,\"err\":{\"message\":"
     +"\"Tacnosttrebabiti100%:expected{tacnost:'0%',greske:[]}todeeplyequal{tacnost:'100%',greske:[]}\"}},"
     +"{\"title\":\"Testovisenemoguizvrsiti\",\"fullTitle\":\"TestoviParserdajTacnost()Testovisenemoguizvrsiti"
     +"\",\"file\":null,\"duration\":1,\"currentRetry\":0,\"err\":{\"message\":\"Testovisenemoguizvrsiti:"
     +"expected{Object(tacnost,greske)}todeeplyequal{Object(tacnost,greske)}\"}},{\"title\":\"Jedantestprolazi\""
     +",\"fullTitle\":\"TestoviParserdajTacnost()Jedantestprolazi\",\"file\":null,\"duration\":0,"
     +"\"currentRetry\":0,\"err\":{\"message\":\"Tacnosttrebabiti50%:expected'33.5%'toequal'33.3%'\"}}],"
     +"\"passes\":[]}");
     let objekat4 = {"tacnost":"0%","greske":["TestoviParserdajTacnost()Svitestoviprolaze",
     "TestoviParserdajTacnost()Testovisenemoguizvrsiti",
     "TestoviParserdajTacnost()Jedantestprolazi"
    ]};
     assert.equal(objekat4.tacnost,rezultat4.tacnost,"Tacnost je 0%");
     assert.equal(rezultat4.tacnost,"0%","Tacnost treba biti 0%");
     assert.deepEqual(objekat4.greske,rezultat4.greske);
   });
   it('Polovina testova prolazi', function(){
     let rezultat5 = TestoviParser.dajTacnost("{\"stats\":{\"suites\":2,\"tests\":4,\"passes\":2,\"pending\":0,"
     +"\"failures\":2,\"start\":\"2021-11-05T15:00:26.343Z\",\"end\":\"2021-11-05T15:00:26.352Z\",\"duration\":9"
    +"},\"tests\":[{\"title\":\"should draw 3 rows when parameter are 2,3\",\"fullTitle\":"
    +"\"Tabela crtaj() should draw 3 rows when parameter are 2,3\",\"file\":null,\"duration\":1,"
    +"\"currentRetry\":0,\"speed\":\"fast\",\"err\":{}},{\"title\":\"should draw 2 columns in row 2 when "
    +"parameter are 2,3\",\"fullTitle\":\"Tabela crtaj() should draw 2 columns in row 2 when parameter are 2,3"
    +"\",\"file\":null,\"duration\":0,\"currentRetry\":0,\"speed\":\"fast\",\"err\":{\"message\":"
    +"\"AssertionError: Greske trebaju biti prazne\"}},{\"title\":\"should draw 4 rows when parameter are 2,4\","
    +"\"fullTitle\":"
    +"\"Tabela crtaj() should draw 4 rows when parameter are 2,4\",\"file\":null,\"duration\":1,"
    +"\"currentRetry\":0,\"speed\":\"fast\",\"err\":{}}"
    +"],\"pending\":[],"
    +"\"failures\":[{\"title\":\"should draw 2 columns in row 2 when "
    +"parameter are 2,3\",\"fullTitle\":\"Tabela crtaj() should draw 2 columns in row 2 when parameter are 2,3"
    +"\",\"file\":null,\"duration\":0,\"currentRetry\":0,\"speed\":\"fast\",\"err\":{\"message\":"
    +"\"AssertionError: Greske trebaju biti prazne\"}},{\"title\":\"should draw 3 rows when parameter are 2,3\",\"fullTitle\":"
    +"\"Tabela crtaj() should draw 3 rows when parameter are 2,3\",\"file\":null,\"duration\":1,"
    +"\"currentRetry\":0,\"speed\":\"fast\",\"err\":{\"message\":\"AssertionError: Greske trebaju biti prazne\"}}],\"passes\":["
    +"{\"title\":\"should draw 2 columns in row 2 when "
    +"parameter are 2,3\",\"fullTitle\":\"Tabela crtaj() should draw 2 columns in row 2 when parameter "
    +"are 2,3\",\"file\":null,\"duration\":0,\"currentRetry\":0,\"speed\":\"fast\",\"err\":{}},"
    +"{\"title\":\"should draw 5 rows when parameter are 3,5\",\"fullTitle\":"
    +"\"Tabela crtaj() should draw 5 rows when parameter are 3,5\",\"file\":null,\"duration\":1,"
    +"\"currentRetry\":0,\"speed\":\"fast\",\"err\":{}}]}");
    let objekat5 = {"tacnost":"50%",
    "greske":["Tabela crtaj() should draw 2 columns in row 2 when parameter are 2,3",
     "Tabela crtaj() should draw 3 rows when parameter are 2,3"]};
     assert.equal(objekat5.tacnost,rezultat5.tacnost,"Jednake tacnosti");
     assert.deepEqual(objekat5.greske,rezultat5.greske,"Greske trebaju biti iste");
     assert.equal(rezultat5.tacnost,"50%","Tacnost treba biti 50%");
   });
   it('Tacnost se zaokruzuje ispravno', function(){
      let rezultat6 = TestoviParser.dajTacnost("{\"stats\":{\"suites\":2,\"tests\":3,\"passes\":2,\"pending\":0,"
      +"\"failures\":1,\"start\":\"2021-11-24T11:19:43.804Z\",\"end\":\"2021-11-24T11:19:43.815Z\","
      +"\"duration\":11},\"tests\":[{\"title\":\"Svitestoviprolaze\",\"fullTitle\":\"TestoviParserdajTacnost()"
      +"Svitestoviprolaze\",\"file\":null,\"duration\":3,\"currentRetry\":0,\"err\":{\"message\":"
      +"\"Tacnosttrebabiti100%:expected{tacnost:'0%',greske:[]}todeeplyequal{tacnost:'100%',greske:[]}\","
      +"\"showDiff\":true,\"actual\":\"{\\n\\\"greske\\\":[]\\n\\\"tacnost\\\":\\\"0%\\\"\\n}\",\"expected\":"
      +"\"{\\n\\\"greske\\\":[]\\n\\\"tacnost\\\":\\\"100%\\\"\\n}\",\"operator\":\"deepStrictEqual\","
      +"\"stack\":\"AssertionError@https:\/\/unpkg.com\/chai\/chai.js:9574:13\\n[3]<\/module.exports\/Assertion"
      +".prototype.assert@https:\/\/unpkg.com\/chai\/chai.js:250:13\\nassertEql@https:\/\/unpkg.com"
      +"zadatak2.js:18:13\\n@file:\/\/\/zadatak2.html:22:12\\n\"}},{\"title\":\"Testovisenemoguizvrsiti\","
      +"\"fullTitle\":\"TestoviParserdajTacnost()Testovisenemoguizvrsiti\",\"file\":null,\"duration\":1,"
      +"\"currentRetry\":0,\"err\":{}},{\"title\":\"Jedantestprolazi\",\"fullTitle\":"
      +"\"TestoviParserdajTacnost()Jedantestprolazi\",\"file\":null,\"duration\":0,\"currentRetry\":0,"
      +"\"err\":{}}],\"pending\":[],"
      +"\"failures\":[{\"title\":\"Svitestoviprolaze\",\"fullTitle\":\"TestoviParserdajTacnost()"
      +"Svitestoviprolaze\",\"file\":null,\"duration\":3,\"currentRetry\":0,\"err\":{\"message\":"
      +"\"Tacnosttrebabiti100%:expected{tacnost:'0%',greske:[]}todeeplyequal{tacnost:'100%',greske:[]}\"}}],"
      +"\"passes\":[{\"title\":\"Jedantestprolazi\""
      +",\"fullTitle\":\"TestoviParserdajTacnost()Jedantestprolazi\",\"file\":null,\"duration\":0,"
      +"\"currentRetry\":0,\"err\":{}},"
      +"{\"title\":\"Testovisenemoguizvrsiti\",\"fullTitle\":\"TestoviParserdajTacnost()Testovisenemoguizvrsiti"
      +"\",\"file\":null,\"duration\":1,\"currentRetry\":0,\"err\":{}}]}");
      let objekat6 = {"tacnost":"66.7%", "greske":["TestoviParserdajTacnost()Svitestoviprolaze"]};
      assert.equal(rezultat6.tacnost,"66.7%","Tacnost treba biti 66.7%");
      assert.deepEqual(objekat6,rezultat6,"Objekti nisu jednaki");
   });
 });
});