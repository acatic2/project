let TestoviParser = (function(){
    const dajTacnost = function(string){
        let objekat, rezultat={"tacnost": null,"greske":[]};
        try {
            objekat = JSON.parse(string);
            let broj = Math.round((((objekat.stats.passes)*1.0/(objekat.stats.tests))*100)*10)/10;
            let tmp = broj;
            if(Math.round(tmp)<100){
            for(let i=0; i<objekat.failures.length;i++)
            rezultat.greske.push(objekat.failures[i].fullTitle);
            }
            if((broj%1) != 0) Math.round(broj);
            rezultat.tacnost = broj + "%";
        } catch (error) {
            rezultat.tacnost='0%';
            rezultat.greske.push('Testovi se ne mogu izvršiti');
            return rezultat;
        } return rezultat;

    }
    const porediRezultate = function(rezultat1, rezultat2){
        let objekat = {"promjena":null,"greske":[]};
        let rez1 = JSON.parse(rezultat1), rez2 = JSON.parse(rezultat2);
        let tmp2 = rezultat2;
        if(rez1.stats.tests==rez2.stats.tests){
            let nizNaziva1 = [] , nizNaziva2 = [] ;
            for( let i=0; i<rez1.stats.tests; i++){
                nizNaziva1.push(rez1.tests[i].fullTitle);
                nizNaziva2.push(rez2.tests[i].fullTitle);
            }
            nizNaziva1.sort();
            nizNaziva2.sort();
            if(JSON.stringify(nizNaziva1)==JSON.stringify(nizNaziva2)){
                objekat.promjena = dajTacnost(tmp2).tacnost;
                for(let i =0; i<rez2.stats.failures; i++){
                    objekat.greske.push(rez2.failures[i].fullTitle);
                }
                objekat.greske.sort();
                return objekat;
            }
        } let paliRez1 = [], testoviRez2 = [], paliRez1Bez2 = [], paliRez2 = [], paliRez2Bez1 = [];
        for(let i=0; i<rez1.stats.failures; i++) 
            paliRez1.push(rez1.failures[i].fullTitle);
        for(let i=0; i<rez2.stats.failures; i++) 
            paliRez2.push(rez2.failures[i].fullTitle);
        for(let i=0; i<rez2.stats.tests; i++) 
            testoviRez2.push(rez2.tests[i].fullTitle);
        let brPalihU1Bez2=0;
        for(let i=0; i<paliRez1.length; i++){
            if(!testoviRez2.includes(paliRez1[i])){
                 brPalihU1Bez2++;
                 paliRez1Bez2.push(paliRez1[i]);
            }
        }
        for(let i=0; i<paliRez2.length; i++){
            if(!paliRez1.includes(paliRez2[i])) paliRez2Bez1.push(paliRez2[i]);
        }
        paliRez1Bez2.sort();
        paliRez2Bez1.sort();
        let x = (brPalihU1Bez2+rez2.stats.failures)/(brPalihU1Bez2+rez2.stats.tests)*100;
        objekat.promjena = x + "%";
        for(let i=0; i<paliRez1Bez2.length;i++) objekat.greske.push(paliRez1Bez2[i]);
        for(let i=0; i<paliRez2Bez1.length;i++) objekat.greske.push(paliRez2Bez1[i]);
            return objekat;
    }
    return{
        dajTacnost : dajTacnost,
        porediRezultate : porediRezultate
    }
}());