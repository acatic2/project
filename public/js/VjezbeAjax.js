let VjezbeAjax = (function(){
    const dodajInputPolja = function(DOMelementDIVauFormi, brojVjezbi) {
        DOMelementDIVauFormi.innerHTML="";
        for (let i=0;i<brojVjezbi;i++) {
           let input = document.createElement("INPUT");
           let labela = document.createElement("label");
           labela.innerHTML = "Broj zadataka za vježbu "+(i+1)+": ";
            input.setAttribute("type", "number");
            input.setAttribute("name","z"+i);
            input.setAttribute("id","z"+i);
            input.setAttribute("value", "4");
            DOMelementDIVauFormi.appendChild(labela);
            DOMelementDIVauFormi.appendChild(input);
            DOMelementDIVauFormi.appendChild(document.createElement("br"));
        }
    }
    const posaljiPodatke = function(vjezbeObjekat, callbackFja) {
        let zadaci = VjezbeObjekat.brojZadataka;
        for (let i=0; i<zadaci.length; i++) {
            if (zadaci[i] < 0 || zadaci[i]>10 || vjezbeObjekat.brojVjezbi!=zadaci.length) {
                console.log("Neispravni podaci");
                return;
        }

        $.ajax({
            url:"http://localhost:3000/vjezbe",
            type: "POST",
            data: VjezbeObjekat,
            success: function(podaci){
                callbackFja(null, podaci);
            }, 
            failure: function(err) {
                callbackFja(err, null);
            }
    
        });   
    }
        }
        const iscrtajVjezbe = function(divDOMelement, vjezbeObjekat) {
            let brojVjezbi = vjezbeObjekat.brojVjezbi;
            let brojZadataka = vjezbeObjekat.brojZadataka;
            let ispravno = true;
            if (brojVjezbi!=brojZadataka.length || brojVjezbi>15 || brojVjezbi<1) 
                ispravno=false;
                for (let i=0; i<brojZadataka.length; i++) {
                    if (brojZadataka[i]<0 || brojZadataka[i]>10) {
                        ispravno = false;
                    }
                }
            if (ispravno) {
                for (let i=0; i<brojVjezbi; i++){
                    let divVjezbe = document.createElement("div");
                    divVjezbe.setAttribute("class","tip1");
                    divVjezbe.innerHTML = "VJEŽBA " + (i+1);
                    divVjezbe.addEventListener('click', function(){
                         iscrtajZadatke(divVjezbe, brojZadataka[i]);
                    });
                    divDOMelement.appendChild(divVjezbe);
                }
            } 
        }
        const iscrtajZadatke = function(vjezbaDOMelement, brojZadataka) {
            let brojVjezbi = vjezbaDOMelement.parentElement.children.length;
            for (let i=0;i<brojVjezbi;i++){
                if (vjezbaDOMelement.parentElement.children[i].firstElementChild != null) {
                    if(vjezbaDOMelement.parentElement.children[i].firstElementChild.style.display != "none") {
                    vjezbaDOMelement.parentElement.children[i].firstElementChild.style.display = "none";
                    break;
                    }
                }
            }
            if (vjezbaDOMelement.lastElementChild != null) {
                if(vjezbaDOMelement.lastElementChild.style.display != "none")
                    vjezbaDOMelement.lastElementChild.style.display = "none";
                else vjezbaDOMelement.lastElementChild.style.display = "block";
            }else {
            let divZadaci = document.createElement("div");
            divZadaci.setAttribute("class", "tip2");
            for (let i=0; i<brojZadataka; i++) {
                let zadatak = document.createElement("button");
                zadatak.textContent = "ZADATAK " + (i+1);
                divZadaci.appendChild(zadatak);
            } vjezbaDOMelement.appendChild(divZadaci);
            }
        }

        const dohvatiPodatke = function(callbackFja) {
            $.ajax({
                url:"http://localhost:3000/vjezbe",
                type: "GET",
                success: function(podaci){
                    callbackFja(null, podaci);
                }, 
                failure: function(greska) {
                    callbackFja(greska, null);
                }
            });
        }
        return{
            dodajInputPolja : dodajInputPolja,
            posaljiPodatke : posaljiPodatke,
            iscrtajVjezbe : iscrtajVjezbe,
            iscrtajZadatke : iscrtajZadatke,
            dohvatiPodatke : dohvatiPodatke
        }
    }());
