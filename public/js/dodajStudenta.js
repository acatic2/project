 document.getElementById("dodaj").addEventListener('click', function(){
    let rezultat = document.getElementById("ajaxStatus"); 
    rezultat.innerHTML = "Ajax status: ";
    let ime = document.getElementById("ime").value;
    let prezime = document.getElementById("prezime").value;
    let index = document.getElementById("index").value;
    let grupa = document.getElementById("grupa").value;
    let student = {ime:ime, prezime:prezime, index:index, grupa:grupa};
        StudentAjax.dodajStudenta(student, function(err, data) {
            if (data!=null)
                rezultat.innerHTML += JSON.stringify(data) ;
            else rezultat.innerHTML += JSON.stringify(err) ;
        });
});
