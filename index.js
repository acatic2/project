const express = require('express');
const bodyParser = require('body-parser');
const fs = require('fs');
const app = express();
app.use(bodyParser.json());

const Sequelize = require("sequelize");
const baza= require('./baza.js');
app.use(express.static(__dirname))

app.use(bodyParser.urlencoded({extended:true }));
app.use(express.static('public'));
app.use(express.static('public/html'));
app.use(express.static('public/css'));
app.use(express.static('public/js'));
app.use(express.static('models'));

baza.sequelize.sync();

async function dajVjezbe() {
    const upit = 'SELECT naziv from vjezba';
    var number = await connection.query(upit,{type: connection.QueryTypes.SELECT});
    return number.length;
}
async function dajZadatke () {
    let niz = [];
    const upit = 'SELECT brojZadataka from vjezba';
    var number = await connection.query(upit,{type:connection.QueryTypes.SELECT});
    for(let i=0 ; i<number.length; i++) 
        niz.push(number[i].brojZadataka);
    return niz;
}

app.get('/vjezbe/', async function(req,res){
res.setHeader('Content-Type','application/json');
let brojVjezbi = await dajVjezbe(), brojZadataka = await dajZadatke();
res.json({brojVjezbi:brojVjezbi,brojZadataka:brojZadataka});
});
app.post('/vjezbe', async function(req, res){
   
         let nizGreski = [], greske = "";
        let brojZadataka = req.body['brojZadataka'].toString().split(',');
        if(parseInt(req.body['brojVjezbi'])<1 || parseInt(req.body['brojVjezbi'])>15)
        nizGreski.push("brojVjezbi");
        for (let i=0; i<brojZadataka.length; i++){
        if(parseInt(brojZadataka[i])<0 || parseInt(brojZadataka[i]>10)) {
            nizGreski.push("z"+i);
        }
    }
    if (req.body['brojVjezbi']!=brojZadataka.length)
    nizGreski.push("brojZadataka");

    for (let i=0; i<nizGreski.length; i++) {
        if (i!=nizGreski.length-1)
            greske += nizGreski[i]+",";
        else 
            greske += nizGreski[i];
    }
    if (greske.length>0) {
        res.send({status : "error", data: "Pogresan parametar "+greske});
        return;
    }
    let zadaci = req.body['brojZadataka'].split(",");
    for (let i=0; i<req.body['brojVjezbi']; i++) {
        let vjezba = "Vjezba " + (i+1);
        let upit = "INSERT INTO VJEZBA(naziv,brojZadataka)VALUES('"+vjezba+"'"+","+
        parseInt(zadaci[i])+")";
        await connection.query(upit,{type: connection.QueryTypes.INSERT});
    }
});

app.post('/student', function(req, res){
    let ime = req.body['ime'];
    let prezime = req.body['prezime'];
    let index = req.body['index'];
    let grupa = req.body['grupa'];
    if (ime=="" || prezime=="" || index=="" || grupa=="" ||
    ime==null || prezime==null || index==null || grupa==null) {
    res.send({status: "Neispravni podaci"});
    return;
}

    baza.Student.findOne({where: {index: index}}).then(function(student){
        if(student==null){
            baza.Student.create({ime: ime, prezime: prezime, index: index}).then(function(){
                baza.Grupa.findOne({where: {naziv: grupa}}).then(function(group){
                    if(group==null)
                        baza.Grupa.create({naziv : grupa});
                        baza.Student.update({ grupa: grupa },{ where: { index : index } });
                    })
                res.send({status: "Kreiran student!"});
            })
        }
        else{
            res.send({status: "Student sa indexom {" + index + "} već postoji!"});
        }
    }).catch(function(err){
        console.log(err)
    })
})

app.put('/student/:index', function(req, res){
    let index = req.params['index'];
    let grupa = req.body.grupa;
  /*  if (index=="" || grupa=="" || index==null || grupa==null) {
        res.send({status: "Neispravni podaci"});
        return;
}*/
    baza.Student.findOne({where: {index: index}}).then(function(student){
        if(student==null){
            res.send({status: "Student sa indexom {"+index+"} ne postoji"});
            return;
        }
        else{
           baza.Grupa.findOrCreate({ where: {naziv : grupa} });
            baza.Student.update({ grupa: grupa },{ where: { index : index } }).then(result => {
                  res.send({status:"Promjenjena grupa studentu {"+index+"}"});
                }).catch(function(err){
                    console.log(err);
                });
            
        /*  student.update({grupa : grupa}).then(result => {
            baza.Grupa.findOne({where: {naziv: grupa}}).then(function(group){
                if(group==null)
                    baza.Grupa.create({naziv : grupa});
            })
              res.send({status:"Promjenjena grupa studentu {"+index+"}"});
            }).catch(function(err){
                console.log(err);
            });*/
        }
    }).catch(function(err){
        console.log(err);
    })
    
})

app.post('/batch/student', bodyParser.text({type: '*/*'}) , function(req, res){
    let tekst = req.body;
    let nizStudenata = tekst.split("\n");
  let   brojDodanih = nizStudenata.length, brojPostojecih = nizStudenata.length, postojeciIndeksi = [];
    for (let i = 0; i<nizStudenata.length; i++) {
        let studenti = nizStudenata[i].split(",");
        let ime = studenti[0], prezime = studenti[1], index = studenti[2], grupa = studenti[3];
     //   console.log(ime+" "+prezime+" "+index+" "+grupa);
        baza.Grupa.findOrCreate({where : {naziv : grupa}});
        baza.Student.findOne({where: {index: index}}).then(function(student){
            if(student==null){
                baza.Student.create({ime: ime, prezime: prezime, index: index, grupa: grupa});//.then(function() {
                 //   baza.Grupa.findOrCreate({where: {naziv: grupa}});
                 //   baza.Student.update({ grupa: grupa },{ where: { index : index } });
              //   baza.Grupa.findOne({where: {naziv: grupa}}).then(function(group){
                 //   if(group==null)
                        baza.Student.update({ grupa: grupa },{ where: { index : index } });
                        brojPostojecih--;
                //    })
          /*      }).catch(function(err){
                    console.log(err);
                });*/
            }
            else{
               // baza.Student.update({ grupa: grupa },{ where: { index : index } });
                brojDodanih--;
                if (!postojeciIndeksi.includes(index))
                postojeciIndeksi.push(index);
            }
        }).catch(function(err){
            console.log(err)
        })
    } console.log(brojPostojecih);
    console.log(brojDodanih);
    console.log(postojeciIndeksi);
     if (brojPostojecih == 0 ) res.send({status: "Dodano {"+brojDodanih+"} studenata!"});
      else {
          let rezultat = "";
        for (let i =0; i<postojeciIndeksi.length; i++) {
            if (i < postojeciIndeksi.length-1)
            rezultat += postojeciIndeksi[i]+",";
            else rezultat += postojeciIndeksi[i];
        }
        res.send({status: "Dodano {"+brojDodanih+"} studenata, a studenti {"+rezultat+"} već postoje!"});
     }
});

app.listen(3000);